<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes([
    'verify' => true
]);
Route::get('/', function () {
    return redirect(route('login'));
});
Route::prefix('/')->middleware('auth', 'verified')->group(function () {
    Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::prefix('mahasiswa')->group(function () {
        Route::get('/', [App\Http\Controllers\MahasiswaController::class, 'index'])->name('mahasiswa.index');
        Route::get('create-mahasiswa', [App\Http\Controllers\MahasiswaController::class, 'create'])->name('mahasiswa.create');
    });

    Route::prefix('buku')->group(function () {
        Route::get('/', [App\Http\Controllers\BukuController::class, 'index'])->name('buku.index');
        Route::get('create-buku', [App\Http\Controllers\BukuController::class, 'create'])->name('buku.create');
    });

    Route::prefix('transaksi')->group(function () {
        Route::get('/', [App\Http\Controllers\TransaksiController::class, 'index'])->name('transaksi.index');
        Route::get('create',[App\Http\Controllers\TransaksiController::class,'create'])->name('transaksi.create');
    });
});
