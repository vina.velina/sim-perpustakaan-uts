@extends('admin.layouts.app')
@section('title', 'Create Mahasiswa')
@section('content')
    <div class="container-fluid">

        <div class="d-flex align-items-center justify-content-between mb-4">
            <h1 class="h4 mb-0">Create Mahasiswa</h1>
            <a href="{{ route('mahasiswa.index') }}" class="btn btn-danger btn-sm btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Back To List</span>
            </a>
        </div>
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-primary">Create Data Mahasiswa</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample">
                <div class="card-body">
                    <form class="user" method="POST">
                        <div class="form-group">
                            <input id="name" type="text" class="form-control form-control-user" name="name" required
                                autocomplete="name" autofocus placeholder="Nama Mahasiswa">
                        </div>
                        <div class="form-group">
                            <input id="email" type="email" class="form-control form-control-user" name="email" required
                                autocomplete="email" autofocus placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input id="no_telp" type="number" class="form-control form-control-user" name="no_telp" required
                                autocomplete="no_telp" autofocus placeholder="Nomor Telepon">
                        </div>

                        <div class="form-group">
                            <input id="prodi" type="text" class="form-control form-control-user" name="prodi" required
                                autocomplete="prodi" autofocus placeholder="Prodi">
                        </div>
                        <div class="form-group">
                            <input id="jurusan" type="text" class="form-control form-control-user" name="jurusan" required
                                autocomplete="jurusan" autofocus placeholder="Jurusan">
                        </div>
                        <div class="form-group">
                            <input id="fakultas" type="text" class="form-control form-control-user" name="fakultas" required
                                autocomplete="fakultas" autofocus placeholder="Fakultas">
                        </div>
                        <a href="{{ route('mahasiswa.index') }}"><button type="button"
                                class="btn btn-secondary" data-dismiss="modal">Close</button></a>
                        <button type="submit" class="btn btn-primary">Save Data</button>

                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
