@extends('admin.layouts.app')
@section('title', 'Create Buku')
@section('content')
    <div class="container-fluid">

        <div class="d-flex align-items-center justify-content-between mb-4">
            <h1 class="h4 mb-0">Create Buku</h1>
            <a href="{{ route('buku.index') }}" class="btn btn-danger btn-sm btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Back To List</span>
            </a>
        </div>
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-primary">Create Data Buku</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample">
                <div class="card-body">
                    <form class="user" method="POST">
                        <div class="form-group">
                            <input id="judul_buku" type="text" class="form-control form-control-user" name="judul_buku"
                                required autocomplete="judul_buku" autofocus placeholder="Judul Buku">
                        </div>
                        <div class="form-group">
                            <input id="pengarang" type="text" class="form-control form-control-user" name="pengarang"
                                required autocomplete="pengarang" autofocus placeholder="Pengarang">
                        </div>
                        <div class="form-group">
                            <input id="penerbit" type="text" class="form-control form-control-user" name="penerbit" required
                                autocomplete="penerbit" autofocus placeholder="Penerbit">
                        </div>
                        <div class="form-group">
                            <input id="tahun" type="number" class="form-control form-control-user" name="tahun" required
                                autocomplete="tahun" autofocus placeholder="Tahun Terbit">
                        </div>
                        <div class="form-group">
                            <input id="tebal" type="number" class="form-control form-control-user" name="tebal" required
                                autocomplete="tebal" autofocus placeholder="Tebal Buku">
                        </div>
                        <div class="form-group">
                            <input id="isbn" type="number" class="form-control form-control-user" name="isbn" required
                                autocomplete="isbn" autofocus placeholder="ISBN">
                        </div>
                        <div class="form-group">
                            <input id="stok_buku" type="number" class="form-control form-control-user" name="stok_buku" required
                                autocomplete="stok_buku" autofocus placeholder="Stok Buku">
                        </div>
                        <div class="form-group">
                            <input id="biaya" type="number" class="form-control form-control-user" name="biaya" required
                                autocomplete="biaya" autofocus placeholder="Biaya Sewa Harian">
                        </div>
                        <a href="{{ route('buku.index') }}"><button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close</button></a>
                        <button type="submit" class="btn btn-primary">Save Data</button>

                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
