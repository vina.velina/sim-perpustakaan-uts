@extends('admin.layouts.app')
@section('title', 'Create Transaksi')
@section('content')
    <div class="container-fluid">

        <div class="d-flex align-items-center justify-content-between mb-4">
            <h1 class="h4 mb-0">Create Transaksi</h1>
            <a href="{{ route('transaksi.index') }}" class="btn btn-danger btn-sm btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-arrow-left"></i>
                </span>
                <span class="text">Back To List</span>
            </a>
        </div>
        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseCardExample">
                <h6 class="m-0 font-weight-bold text-primary">Create Data Transaksi</h6>
            </a>
            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample">
                <div class="card-body">
                    <form class="user" method="POST">
                        <div class="form-group">
                            <select id="mahasiswa" class="form-control form-control-user" name="mahasiswa" required
                                autocomplete="mahasiswa" autofocus>
                                <option value="">-- Pilih Mahasiswa -- </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select id="buku" class="form-control form-control-user" name="buku" required
                                autocomplete="buku" autofocus>
                                <option value="">-- Pilih Buku -- </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input id="tanggal_pinjam" type="date" class="form-control form-control-user"
                                name="tanggal_pinjam" required autocomplete="tanggal_pinjam" autofocus>
                        </div>
                        <a href="{{ route('transaksi.index') }}"><button type="button" class="btn btn-secondary"
                                data-dismiss="modal">Close</button></a>
                        <button type="submit" class="btn btn-primary">Save Data</button>

                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
