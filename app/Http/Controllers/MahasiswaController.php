<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index()
    {
        return view('admin.pages.mahasiswa.index', ['id_page' => 'mahasiswa']);
    }

    public function create()
    {
        return view('admin.pages.mahasiswa.create', ['id_page' => 'mahasiswa']);
    }
}
