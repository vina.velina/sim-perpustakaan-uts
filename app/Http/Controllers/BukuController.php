<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BukuController extends Controller
{
    public function index()
    {
        return view('admin.pages.buku.index', ['id_page' => 'buku']);
    }

    public function create()
    {
        return view('admin.pages.buku.create', ['id_page' => 'buku']);
    }
}
