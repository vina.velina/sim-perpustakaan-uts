<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    public function index()
    {
        return view('admin.pages.transaksi.index', ['id_page' => 'transaksi']);
    }

    public function create()
    {
        return view('admin.pages.transaksi.create', ['id_page' => 'transaksi']);
    }
}
